#include <stdio.h>
int main()
{
   char s[1000], r[1000];
   int start, end, i = 0;

   printf("Enter the string:\n");
   scanf("%[^\n]%*c", s);

   while (s[i] != '\0')
      i++;

   end = i - 1;

   for (start = 0; start < i; start++) {
      r[start] = s[end];
      end--;
   }

   r[start] = '\0';

   printf("%s\n", r);

   return 0;
}
